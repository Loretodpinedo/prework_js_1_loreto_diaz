//
var numero1 = 5;
var numero2 = 2;

function miFuncionDeComparar (numero1){
    if(numero1 === 5){
        console.log(numero1 + " es estrictamente igual a 5");
    }else{
        console.log(numero1 + " no es estrictamente igual a 5");
    }
}

function miFuncionDeComparar1 (numero1, numero2){
    if (numero1 <= numero2){
        console.log(numero1 + " no es mayor que " + numero2);
    }else{
        console.log(numero1 + " es mayor que " + numero2); 
    }
}

function miFuncionDeComparar2 (numero2){
    if (numero2 < 0){
        console.log(numero2 + " es negativo ");
    }else{
        console.log(numero2 + " no es negativo"); 
    }
}

function miFuncionDeComparar3 (numero1){
    if (numero1 < 0 || numero1 != 0){
        console.log(numero1 + " es negativo o distinto de cero");
    }else{
        console.log(numero1 + " es positivo o igual a cero"); 
    }
}

miFuncionDeComparar(5);
miFuncionDeComparar(-1);
miFuncionDeComparar1(5, 10);
miFuncionDeComparar1(5, 3);
miFuncionDeComparar2(-3);
miFuncionDeComparar2(6);
miFuncionDeComparar3(-9);
miFuncionDeComparar3(0);
miFuncionDeComparar3(1);
